import React from "react"
import Image from "../images/image_vector.svg"
import LinkRow from "../components/button_row"
import { Link } from "gatsby"

const AboutSection = () => (
  <section id="ueber" class="wrapper">
    <div className="padded lg">
    <h2>Wie funktioniert das Rundholz&shy;portal?</h2>
      <div className="wrapper">
        <div className="block50">
          <p>Das Rundholzportal verbessert die Kommunikation zwischen Waldbesitzern, Spediteuren und Holzverarbeitern. Hier haben Sie alles in einem Konto. Sie werden anhand Ihrer Kundennummer mit Ihren Holzverarbeitern verknüpft.</p>
          <p><b>Waldbesitzer</b> melden geplante Liefermengen, erhalten Preisangebote und sehen den Stand aktueller Kaufverträge und Lieferpläne. Über das Rundholzportal können Holzlisten einzelnen Holzverarbeitern direkt bereitgestellt und Abrechnungen zu Holzbereitstellungen aufgerufen werden. </p>
          <p><b>Spediteure</b> haben alle Bestellungen auf einem Blick. Gutschriften und Lieferscheine sind jederzeit abrufbar. Alle Fahrer einer Spedition haben Zugriff auf die Transportaufträge und können ihre Touren effizienter planen. Fahrer einer Spedition sind untereinander verbunden und haben immer den aktuellen Stand. Lieferscheine in Papierform gehören der Vergangenheit an.</p>
          <p>Ein Prozess für alle Beteiligten: digital, per <Link to='#app'>App</Link> oder Online; ohne unnötiges Papier.</p>
        </div>

        <div className="block50 images">
          <img src={Image} alt="Rundholzportal regelt alles zentral"></img>
        </div>
      </div>
      <LinkRow></LinkRow>
    </div>
  </section>
)

export default AboutSection
