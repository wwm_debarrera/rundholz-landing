import React from "react"
import "../styles/site.scss"

import Layout from "../components/layout"
import SEO from "../components/seo"

import StartSection from "./start"
import AboutSection from "./about"
import SignUpSection from "./signup"
import InfoSection from "./info"
import AppSection from "./app"

const IndexPage = () => (
  <Layout>
    <SEO title="Home"/>
    <StartSection></StartSection>
    <AboutSection></AboutSection>
    <InfoSection></InfoSection>
    <SignUpSection></SignUpSection>
    <AppSection></AppSection>
  </Layout>
)

export default IndexPage
