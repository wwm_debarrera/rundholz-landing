import React from "react"

const AppSection = () => (
  <section id="app" class="wrapper">
    <div className="padded lg">
      <h2>Auch als App</h2>
      <div className="wrapper padded">
        <p className="row text-center" >Die App für Waldbesitzer, Fahrer und Dienstleister für Android und iOS.</p>
        <a href="https://play.google.com/" target="_new" className="block50 wrapper">
          <img src={'../../google-play-badge.png'} alt="Jetzt bei Google Play" className="block-content"></img>
        </a>
        <a href="https://www.apple.com/de/ios/app-store/" target="_new" className="block50 wrapper">
          <img src={'../../appstore-badge.png'} alt="Laden im App Store" className="block-content"></img>
        </a>
      </div>
    </div>
  </section>
)

export default AppSection
