import React from "react"
import { Link } from "gatsby"

const StartSection = () => (
  <section className="wrapper full-height" id="start">
    <div className="padded lg wrapper ">
      <div className="main-claim">
        <h1>Willkom&shy;men auf dem Rundholzportal</h1>
        <h4>Die Kommunikationsplattform für Waldbesitzer, Spediteure und Holzverarbeiter</h4>
      </div>
      <div className="cta-block sticky spaced">
        <Link to='#registrierung' className="button cta">Anmelden</Link>
        <Link to='#registrierung' className="button">Registrieren</Link>
      </div>
    </div>
  </section>
)

export default StartSection
