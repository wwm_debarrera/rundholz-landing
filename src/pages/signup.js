import React from "react"
import { Link } from "gatsby"

const SignUpSection = () => (
  <section id="registrierung" className="wrapper">
    <div className="padded lg">
    <h2>Wie kann ich teilnehmen?</h2>
      <div className="wrapper">
          <p>Spediteure und Waldbesitzer können sich jederzeit auf dem Rundholzportal registrieren. Die Zuordnung zu Ihrem Holzverarbeiter erfolgt mit Ihrer Kundennummer. Diese geben Sie im Anschluss an die Registrierung ein, dass Ihr Konto mit dem Ihres Verarbeiters verknüpft werden kann. Fertig.</p>

          <div className="row wrapper spaced border">
            <div className="block25">
              <div className="card"><span>1</span> Registrieren</div>
            </div>
            <div className="block25">
              <div className="card"><span>2</span> Konto zuordnen</div>
            </div>
          </div>


        <div className="row sticky">
        <Link to='#registrierung' className="button cta float-right">Jetzt registrieren</Link>
        </div>
      </div>
    </div>
  </section>
)

export default SignUpSection
