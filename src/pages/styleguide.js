import React from "react"
import LogosSection from "./styleguide/logos"
import ElementsSection from "./styleguide/elements"
import TypographySection from "./styleguide/typography"
import ColorsSection from "./styleguide/colors"


const Styleguide = () => (
  <>
    <LogosSection />
    <TypographySection />
    <ElementsSection />
    <ColorsSection />
  </>
)

export default Styleguide
