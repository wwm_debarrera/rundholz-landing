import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { Link } from "gatsby"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Nicht gefunden" />
    <section class=" full-height">
      <div class="wrapper padded">
        <h1>Die angefragte Seite existiert nicht</h1>
        <div class="row padded">
          <Link to='/' className="button float-right cta">Zurück</Link>
        </div>
      </div>
    </section>
  </Layout>
)

export default NotFoundPage
