import React from "react"
import LinkRow from "../components/button_row"

const InfoSection = () => (
  <section id="info" class="wrapper">
    <div class="padded lg">
      <h2>Wer ist Betreiber des Rundholz&shy;portals?</h2>
      <div class="wrapper">
        <div class="block50">
          <h3>Idee und Gründung</h3>
          <p>Das Portal wird finanziert von den Holzverarbeitern selbst. Die Anschubfinanzierung wurde durch die
            <a href="https://www.pollmeier.com/de/" target="_new"> Pollmeier Massivholz GmbH & Co. KG </a>
            geleistet. Die Umsetzung und technische Betreuung erfolgt durch die Firma
            <a href="https://www.intend.de/" target="_new"> INTEND Geoinformatik GmbH. </a></p>
          <p>Die Teilnahme für <b>Waldbesitzer und Spediteure ist kostenlos.</b></p>
        </div>

        <div class="block50">
          <h3>Die weitere Entwicklung</h3>
          <p>Jeder Holzverarbeiter finanziert die eigene Schnittstelle, über die mit dem Rundholzportal und den teilnehmenden Spediteuren und Waldbesitzern kommuniziert wird. Die Teilnahme für <b>Waldbesitzer und Spediteure ist kostenlos.</b></p>
          <p>Weitere Funktionalitäten werden in Abstimmung mit allen Anwendergruppen bewertet und programmiert.</p>
        </div>

        <div class="block50">
          <h3>Was passiert mit meinen Daten?</h3>
          <p>Die Datenhoheit liegt bei Ihnen. Sie entscheiden selbst, mit wem Sie Daten teilen möchten.</p>
        </div>

        <div class="block50">
          <h3>Sind die Daten sicher?</h3>
          <p>Ihre Daten werden nach aktuellen Sicherheitsstandards und nach Konformität mit der <b>DSGVO</b> gespeichert.</p>
        </div>

      </div>
      <LinkRow></LinkRow>
    </div>
  </section>
)

export default InfoSection
