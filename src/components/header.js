import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import logo from "../images/pollmeier-logo.png"
//import NavItems from "./nav_items"


const Header = ({ siteTitle }) => (

  <header>
  <Link to="/" className="logo-wrapper">
    <button className="burger"></button>
    <img className="logo" src={logo} alt="Logo" />
  </Link>
  <div className="wrapper padded">
  <Link to="/vorteile"> vorteile </Link>
  <Link to="/holz"> holz </Link>
  <Link to="/praemien"> prämien </Link>
  </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
