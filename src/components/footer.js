import { Link } from "gatsby"
import React from "react"


const Footer =({siteTitle}) => (

  <footer className="wrapper">
    <div className="wrapper padded">
      <span>©{new Date().getFullYear()} Pollmeier Massivholz GmbH & Co.KG</span>
      <Link to="/page-2">Impressum</Link>
      <Link to="/page-2">Datenschutz</Link>
    </div>
  </footer>
)

export default Footer
