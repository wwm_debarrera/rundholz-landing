import React from "react"
import { Link } from "gatsby"

const LinkRow = () => (
<div class="row">
  <Link to='#registrierung' className="button float-right">Registrieren</Link>
</div>
)

export default LinkRow
