/**
* Layout component that queries for data
* with Gatsby's useStaticQuery component
*
* See: https://www.gatsbyjs.org/docs/use-static-query/
*/

import React from "react"
import PropTypes from "prop-types"

import Footer from "./footer"
import Logo from "../images/logo.svg"
const Layout = ({ children }) => {

  if (typeof window !== "undefined") {
   // eslint-disable-next-line global-require
   require("smooth-scroll")('a[href*="#"]')
 }

    return (
      <>
      <main>
      <img id="Logo" src={Logo} alt="Rundholzportal Logo"></img>
      {children}
      </main>
      <Footer />
      </>
    )
  }

  Layout.propTypes = {
    children: PropTypes.node.isRequired,
  }

  export default Layout
